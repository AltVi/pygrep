"""The main functions of the search utility

 *functions aviable:
    - write_result(message) - takes text(message), returns
    the list of the lines contents

    - search_in_file(template, file_path, recursive=False, show_numbers=False,
                     word_search=False, reverse=False) - Search in a file from a
    template with the specified parameters.

    - check_word_in_line(template, line) - Looks for a pattern as a word in a string,
    returns the truth when it is found

    - search_in_lines(template, file, reverse=False) - This is a generator that returns
    lines that match (not suitable) the pattern
"""
import re
import os


def write_result(message):
    """Gets the text. Removes the empty string and returns a list of strings"""
    line_list = []
    for line in message.split("\n"):
        if line.strip() != "":
            line_list.append(line.strip())
    return line_list


def search_in_file(template, file_path, recursive=False, show_numbers=False,
                   word_search=False, reverse=False):
    """Search for a template in a file. When recursion=True, it also looks in the directory"""
    final_message = ""
    message = ""
    count = 0
    if os.path.isfile(file_path):
        for line, number in search_in_lines(template, file_path, reverse):
            count = count + 1
            if word_search:
                if not check_word_in_line(template, line):
                    break
            if show_numbers:
                message = " ".join([str(number), line])
            else:
                message = line

            message =  ":".join([file_path, message])
            final_message = "\n".join([final_message, message])
    elif os.path.isdir(file_path) and recursive:
        files_in_dir = os.listdir(file_path)
        for file_in_dir in files_in_dir:
            file_in_dir = os.path.join(file_path, file_in_dir)
            message = search_in_file(template, file_in_dir, recursive, show_numbers, word_search, reverse)
            final_message = "\n".join([final_message, message])

    return final_message


def check_word_in_line(template, line):
    """Replaces all punctuation marks in the line with spaces. Searches for a pattern
    with spaces added at the beginning and end of the template"""
    line = " ".join(["", line, ""])
    line = re.sub(r'[;,\s]', ' ', line)
    template = " ".join(["", template, ""])
    if re.findall(template, line) != []:
        return True
    else:
        return False


def search_in_lines(template, file, reverse=False):
    """A generator that returns a strings ,containing (not containing) a template"""
    count = 0

    with open(file, "r") as text:
        lines = [line.strip() for line in text]
    for line in lines:
        count = count + 1
        if not reverse:
            if re.findall(template, line) != []:
                yield line, count
        else:
            if re.findall(template, line) == []:
                yield line, count