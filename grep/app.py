#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Main module of deleting utility
 *constants aviable:
    - LOG_FORMAT - format of logging

    - LOGGER - object of log

    - TERMINAL_MESSAGE - log for terminal

 *functions aviable:
    - setup_log(adress_log, log_level, silent) - takes adress to which the log
    will be record.
"""
import sys
import argparse
import os
import logging
import search

from config_tools import load_config


LOG_FORMAT = "Time: %(asctime)s  Type: [%(levelname)s] Message: %(message)s"
LOGGER = logging.getLogger(__name__)
TERMINAL_MESSAGE = logging.StreamHandler()


def setup_log(adress_log, log_level="DEBUG"):
    """Function for redefining the log"""
    LOGGER.addHandler(TERMINAL_MESSAGE)
    try:
        LOGGER.setLevel(log_level)
        logging.basicConfig(filename=adress_log, format=LOG_FORMAT)
        return True
    except:
        return False


def main():
    """The starting function of the block.
    performs interrelations between functions and user
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("search", type=str,
                        help="Template for searching and files in which the search will be performed")
    parser.add_argument("files", nargs="+", type=str,
                        help="Template for searching and files in which the search will be performed")
    parser.add_argument("-r", "--recursive", help="Search recursively", action="store_true")
    parser.add_argument("-o", "--output", help="Record result of searching in output.txt")
    parser.add_argument("-n", "--number", action="store_true", help="Adds the line number in which the match is found")
    parser.add_argument("-w", "--word", action="store_true", help="Searches for a query as a word")
    parser.add_argument("-c", "--count", action="store_true", help="Shows the number of matches in the file")
    parser.add_argument("-l", "--list", action="store_true", help="Output files in which the template is found")
    parser.add_argument("--reverse", action="store_true", help="Output of lines in which the template is not found")
    parser.add_argument("--config", nargs=2, help="Load config.Enter parametres(config adress, config format[json/txt])")
    parser.add_argument("--log", help="Log adress")
    parser.add_argument("--level", help="Log level")
    args = parser.parse_args()

    search_template = args.search
    files_to_search = args.files

    info_adress = "/home/droid/python/pygrep/grep/Output.txt"
    log_adress = "/home/droid/python/pygrep/grep/log.log"
    log_level = "DEBUG"
    recursive = False
    show_numbers = False
    word_search = False
    count = False
    write_file_list = False
    reverse = False

    try:
        if args.config:
            config = load_config(args.config[0], args.config[1])
        else:
            config = load_config()

        info_adress = config["info_path"]
        log_adress = config["log_path"]
        log_level = config["log_level"]
        recursive = config["recursive"]
        show_numbers = config["number"]
        word_search = config["word"]
        count = config["count"]
        write_file_list = config["list"]
        reverse = config["reverse"]

        """For txt config"""
        if config["recursive"] == "False":
            recursive = False
        if config["number"] == "False":
            show_numbers = False
        if config["word"] == "False":
            word_search = False
        if config["count"] == "False":
            count = False
        if config["list"] == "False":
            write_file_list = False
        if config["reverse"] == "False":
            reverse = False
    except:
        print "Error in config. Use default configuration"

    if args.recursive:
        recursive = args.recursive
    if args.number:
        show_numbers = args.number
    if args.word:
        word_search = args.word
    if args.count:
        count = args.count
    if args.list:
        write_file_list = args.list
    if args.reverse:
        reverse = args.reverse


    if args.level:
        log_level = args.level
    if args.log:
        log_adress = args.log

    if not os.path.exists(log_adress):
        with open(log_adress, "w"):
            pass

    if not setup_log(log_adress, log_level):
        print "Wrong log information"
        sys.exit()

    if args.output:
        info_adress = args.output
        try:
            with open(info_adress , "w"):
                pass
            message = " ".join(["Write in", info_adress])
            LOGGER.log(20, message)
        except:
            LOGGER.log(50, "Error with output file")
            sys.exit()

    for file in files_to_search:

        test_result = search.search_in_file(search_template, file, recursive, show_numbers,
                                            word_search, reverse)
        line_list = search.write_result(test_result)
        for line in line_list:

            LOGGER.log(10, line)
            if args.output:
                with open(info_adress, "a") as record:
                    line = "".join([line, "\n"])
                    record.write(line)

        if count:
            message = "-->".join([file, str(len(line_list))])
            LOGGER.log(10, message)

        if write_file_list and line_list != []:
            message = ":".join([file, " contains a regular expression"])
            LOGGER.log(10, message)


if __name__ == '__main__':
    main()