"""This module create for testing rm utility"""
import unittest
import shutil
import os
from grep.search import search_in_file, write_result

DEFAULT_TESTDIR_ADRESS = os.path.join(os.path.dirname(__file__), "test_dir")


class TestRMPY(unittest.TestCase):


    def setUp(self):
        os.makedirs(DEFAULT_TESTDIR_ADRESS)


    def tearDown(self):
        shutil.rmtree("test_dir")


    def test_simple_grep(self):
        file = "test_dir/a.txt"
        with open(file, "w") as record:
            record.write("123")
        template = "123"
        result = search_in_file(template, file)
        result = result.split(":")[1]
        self.assertTrue(result == "123")


    def test_recursive_grep(self):
        os.makedirs("test_dir/dir1/dir2/dir3")
        recursive = True
        file1 = "test_dir/dir1/a.txt"
        with open(file1, "w") as record:
            record.write("123")
        file2 = "test_dir/dir1/dir2/b.txt"
        with open(file2, "w") as record:
            record.write("234")
        file3 = "test_dir/dir1/dir2/dir3/c.txt"
        with open(file3, "w") as record:
            record.write("125")
        template = "1"
        result = search_in_file(template, "test_dir", recursive)
        result = write_result(result)

        line = result[0].split(":")[1]
        self.assertTrue(line == "123")
        line = result[1].split(":")[1]
        self.assertTrue(line == "125")


    def test_show_numbers(self):
        file1 = "test_dir/a.txt"
        with open(file1, "w") as record:
            record.write("123\n234\n345")
        template = "4"
        recursive = False
        show_numbers = True
        result = search_in_file(template, file1, recursive, show_numbers)
        result = write_result(result)

        line = result[0].split(":")[1]
        number = int(line.split(" ")[0])
        self.assertTrue(number == 2)

        line = result[1].split(":")[1]
        number = int(line.split(" ")[0])
        self.assertTrue(number == 3)


    def test_word_search(self):
        file1 = "test_dir/a.txt"
        with open(file1, "w") as record:
            record.write("Hello, Helloy")
        file2 = "test_dir/b.txt"
        with open(file2, "w") as record:
            record.write("Helloy")

        template = "Hello"
        recursive = False
        show_numbers = False
        word_search = True

        result = search_in_file(template, file1, recursive, show_numbers, word_search)
        result = write_result(result)
        self.assertTrue(result != [])

        result = search_in_file(template, file2, recursive, show_numbers, word_search)
        result = write_result(result)
        self.assertFalse(result != [])


    def test_reverse_search(self):
        file = "test_dir/a.txt"
        with open(file, "w") as record:
            record.write("111\n222\n113")

        template = "11"
        recursive = False
        show_numbers = False
        word_search = False
        reverse = True

        result = search_in_file(template, file, recursive, show_numbers, word_search, reverse)
        result = write_result(result)
        result = result[0].split(":")[1]
        self.assertTrue(result == "222")


if __name__ == '__main__':
    unittest.main()
