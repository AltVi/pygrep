# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Task(models.Model):
    file_path = models.CharField(max_length=100)

    def __str__(self):
        return self.file_path


