#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import json

CONFIG_JSON_ADRESS = os.path.join(os.path.dirname(__file__), "config/config.json")
CONFIG_TXT_ADRESS = os.path.join(os.path.dirname(__file__), "config/config.txt")


def load_config(config_format="json", config_adress=CONFIG_JSON_ADRESS):
    """Function for loading config file"""
    if config_format == "txt":
        return load_config_txt(config_adress)
    else:
        return load_config_json(config_adress)


def load_config_json(config_adress=CONFIG_JSON_ADRESS):
    """Function for processing json config file"""
    with open(config_adress, "r") as config_file:
        parametres = json.load(config_file)
    return parametres


def load_config_txt(config_adress=CONFIG_TXT_ADRESS):
    """Function for processing txt config file"""
    with open(config_adress, "r") as config_file:
        lines = [line.strip() for line in config_file]
    parametres = {}
    for line in lines:
        values = line.split("=")
        parametres[values[0].strip()] = values[1].strip()
    return parametres
