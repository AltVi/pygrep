
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

from django.template.loader import render_to_string

from grep.search import search_in_file, write_result

@csrf_exempt
def home(requsest):
    result_list = []
    if (requsest.POST):
        regex = requsest.POST.get("regex")
        file_path = requsest.POST.get("file_path")
        result = search_in_file(regex, file_path)
        result_list = write_result(result)
        recursive = requsest.POST.get("recursive")
        print recursive
        #return redirect('/home/')
        context = {
            'list_result': result_list
        }
        return HttpResponse(render_to_string('index.html', context))
    context = {
            'list_result': result_list
        }
    return HttpResponse(render_to_string('index.html', context))
