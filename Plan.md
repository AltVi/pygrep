План работы
 - Парсинг строки из терминала. Нерекурсивная работа. Далее могут добавляться новые параметры.
 - Добавление рекурсии
 - Добавление конфига
 - Добавление логгера
 - Добавление тестов
 - Добавление setup.py
 - Добавление паралельного выполнения
 - Добавление веб-интерфейса