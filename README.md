This is grep utility 
version = 1.0

Quickstart: 

Change paths in config.json(config.txt) and config_tools.py 
Then write "sudo python setup.py install" in the terminal 

Parametres: 

run in default mode , application search template in file: 

    pygrep [path to file] [template]

To work with the directory, use the -r flag: 

    pygrep [path to dir] [template] -r 

To output result to file use -o flag and write path to file: 

    pygrep [path to file] [template] -o [path to output] 

If you want to know the line number in the file, specidy -n flag: 

    pygrep [path to file] [template] -n

If you want search template like a word, write -w flag:

    pygrep [path to file] [template] -w

If you want know the number of matches in the file done: 
 
    pygrep [path to fil] [template] -c 

To output files in which the template is found, use -l flag: 

    pygrep [path to file] [template] -l

To output the lines in which the template is not found, specify --reverse flag: 

    pygrep [path to file] [template] --reverse 

If you want to load config, write:

    pygrep [path to file] [template] --config [format] [adress] 


Config example: 

JSON
{
  "log_path": "/home/droid/python/pygrep/grep/log.log",
  "info_path": "/home/droid/python/pygrep/grep/Output.txt",
  "log_level": "DEBUG",
  "output_file": false,
  "recursive": false,
  "number": false,
  "word": false,
  "count": false,
  "list": false,
  "reverse": false
}

TXT
log_path = /home/droid/python/pygrep/grep/log.log
info_path = /home/droid/python/pygrep/grep/Output.txt
log_level = DEBUG
output_file = False
recursive = False
number = False
word = False
count = False
list = False
reverse = False

params: 
    template - regular expression for searching
    trash - path to trash 
    log_path - path to log file 
    log_level - set level of logging 
    output_file - write result of searching in file
    recursive - search in directory
    number - add numders of lines
    word - search template like a word
    count - write the number of matches in the file
    list - write files in which the template is found
    reverse - search lines which not have template

Modules description:

1 - app.py Represents application's entry point. The place where all the main functions are called. 

2 - search.py - Includes functions for search files.

3 - load_config.py The module loading configuration files. 
